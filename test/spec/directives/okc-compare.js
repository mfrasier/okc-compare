describe('Unit: Directives', function () {
    describe(' okcCompare', function () {
        var scope, form;

        // load our app
        beforeEach(module('myApp'));
        beforeEach(inject(function ($compile, $rootScope, _$log_) {
            scope = $rootScope.$new();
            $log = _$log_;
            /*jshint multistr: true */
            var element = angular.element(
                '<form name=\"personForm\" class=\"\" novalidate >\
                \
                    <fieldset>\
      \
                    <!-- FIRST NAME -->\
                    <div class=\"control-group\">\
                        <label class=\"control-label\">First Name:</label>\
                        <div class=\"controls controls-row\">\
                            <input type=\"text\" name=\"name_first\"\
                            ng-model=\"person.name_first\"\
                            placeholder=\"First Name\"\
                            required ng-minlength=\"2\" ng-maxlength=\"50\"\
                            >\
                            </div>\
                        </div>\
      \
                        <!-- Last NAME -->\
                        <div class=\"control-group\">\
                            <label class=\"control-label\">Last Name:</label>\
                            <div class=\"controls controls-row\">\
                                <input type=\"text\" name=\"name_last\"\
                                ng-model=\"person.name_last\"\
                                placeholder=\"Last Name\"\
                                ng-minlength=\"2\" ng-maxlength=\"50\"\
                                okc-compare=\'[ { "=":"personForm.name_first"} ]\'\
                                >\
                                </div>\
                            </div>\
      \
                            <!-- AGE -->\
                            <div class="control-group">\
                                <label class="control-label">Age:</label>\
                                <div class="controls controls-row">\
                                    <input type="number" name="age"\
                                    ng-model="person.age"\
                                    placeholder="Enter Age"\
                                    min="0" max="140"\
                                    okc-compare=\'[ { "=":"form.personForm.name_first"}, { "==":"personForm.name_first"}, { "+":"personForm.name_first"} ]\'\
                                    >\
                                    </div>\
                                </div>\
      \
                                <!-- OLDER AGE-->\
                                <div class="control-group">\
                                    <label class="control-label">Age:</label>\
                                    <div class="controls controls-row">\
                                        <input type="number" name="age_older"\
                                        ng-model="person.age_olde"\
                                        placeholder="Enter Age"\
                                        min="0" max="140"\
                                        okc-compare=\'[ { ">=":"personForm.age"} ]\'\
                                        >\
                                        </div>\
                                    </div>\
                                </fieldset>\
                            </form>');

            scope.person = {
                name_first : 'Joe',
                name_last  : 'Blow',
                age        : 40
            };

            $compile(element)(scope);
            scope.$digest();
            form = scope.personForm;    // get formController
        }));


        // tests
        it('should have three okc-compare specification errors', function () {
            expect($log.error.logs.length).toBe(3);
            //console.log('three expected errors: ' + $log.error.logs);
        });

        xit('should set $error.okcCompare false when name_last equals name_first', function () {
            // both form... and scope.personForm... seem successful
            console.log($log.log.logs);
            dump(form.name_first);
            dump(form.name_last);
            dump(scope.person);
            scope.$apply(function() {
                form.name_first.$setViewValue('joey');
                //scope.personForm.name_first.$setViewValue('joey');
                form.name_last.$setViewValue('joey');
                //scope.personForm.name_last.$setViewValue('joey');
            });
            console.log($log.log.logs);
            scope.$digest();
            dump(form.name_first);
            dump(form.name_last);
            dump(scope.person);

            expect(form.name_last.$valid).toBe(true);
            expect(form.name_last.$error.okcCompare).toBe(false);
            //expect(scope.personForm.name_last.$error.okcCompare).toBe(false);
        });

        xit('should set $error.okcCompare true when name_last and name_first are different', function () {
            form.name_first.$setViewValue('joey');
            form.name_last.$setViewValue('betty');

            expect(scope.personForm.name_last.$error.okcCompare).toBe(true);
            expect(form.name_last.$error.okcCompare).toBe(true);
        });

        it('should set $error.okcCompare true when age_older is less than age', function () {
            form.age.$setViewValue('40');
            form.age_older.$setViewValue('30');

            expect(scope.personForm.age_older.$error.okcCompare).toBe(true);
        });

        it('should set $error.okcCompare false when age_older is greater than age', function () {
            form.age.$setViewValue('40');
            form.age_older.$setViewValue('50');

            expect(form.name_last.$error.okcCompare).toBe(false);
        });

        it('should set $error.okcCompare false when age_older is equal to age', function () {
            form.age.$setViewValue('40');
            form.age_older.$setViewValue('40');

            expect(form.name_last.$error.okcCompare).toBe(false);
        });

    });
});