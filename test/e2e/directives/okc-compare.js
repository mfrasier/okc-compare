describe('E2E: Directives: ', function () {
    describe('okcCompare', function () {

        beforeEach(function () {
            browser().navigateTo('/#/');
        });

        it('should load the index page', function () {
            browser().navigateTo('/#/');
            expect(browser().location().path()).toBe('/');
        });

        it('should set last_name to joey', function () {
            input('person.name_last').enter('joey');
            expect(input('person.name_last').val()).toBe('joey');
        });

        it('should set age and age_older values', function () {
            input('person.age').enter('40');
            input('person.age_older').enter('56');

            expect(input('person.age_older').val()).toBe('56');
            expect(input('person.age').val()).toBe('40');
        });
    });
});