Todo
======================
1. directive attribute that can be added to a form element
2. directive parameters should accept an array of comparitors: [{'>', 'form.elementname'}, {'=', 'form.element2name'},...]
3. it should evaluate whether this form element value follows the rules given by the comparitors
4. operators: = < > <= >=

Example
======================
<input name="last_name" ng-compare="['=','myForm.name_first']">

see [http://blog.brunoscopelliti.com/angularjs-directive-to-check-that-passwords-match]

http://plnkr.co/edit/kJBIR8cmzPIblIHEwasv?p=preview

https://c9.io/mfrasier/okc-compare