'use strict';
/**
 *
 */

/**
 * function comparatorErrorMsg
 * Build error msg generic portion, appending specific part from caller
 * parameters:
 *   comparator - object containing properties
 *   msg specific portion of error message
 * return:
 *   error message string
 */
var comparatorErrorMsg = function (comparator, msg) {
    var messageInfo = {
        operator: 'unknown',
        value: 'unknown',
        elemName: 'unknown',
        message: msg
    };

    if (comparator.element) {
        try {
            messageInfo.elemName = comparator.element[0].name;
        }
        catch (err) {
            console.error('exception getting element name.  This shouldn\'t occur - check caller.');
            messageInfo.elemName = 'unresolvable';
        }
    }

    if (comparator.operator) {
        messageInfo.operator = comparator.operator;
    }
    else {
        console.error('bad operator parameter');
    }

    if (comparator.nodeSpec) {
        messageInfo.nodeSpec = comparator.nodeSpec;
    }
    else {
        console.error('no comparator parameter');
    }

    return  '[okcCompare]: Error on element named \'' + messageInfo.elemName + '\'' +
            'for comparator \'' + messageInfo.operator + ':' + messageInfo.nodeSpec + '\'. ' +
            messageInfo.message;
};

/**
 * okcCompare directive
 * attribute value is array of single element objects in JSON format
 *   key is operator, value is a form.input element
 *   e.g. [ { "=" : "formName.inputName" } ... ]
 *
 *   TODO if inherited scope, not isolate,
 *     can simplify by directly using form_name.input_name.$viewValue instead of jQuery calls
 */
angular.module('myApp')
    .directive('okcCompare', function ($log) {

        // directive-wide variables
        var validationErrorKey = 'okcCompare',
            validOperators = [
                '=', 'eq',
                '*=', 'eqo',    // value optional, equals if value exists
                '!=', 'ne',
                '>', 'gt',
                '>=', 'gte',
                '<', 'lt',
                '<=', 'lte'
            ];

        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, elem, attrs, ngModel) {
                var okcCompare = attrs.okcCompare,
                    comparators = [ ],
                    goodComparators = [ ];

                if (!ngModel) return;

                // parse attribute JSON string
                try {
                    comparators = JSON.parse(okcCompare);
                }
                catch (err) {
                    // show error and leave
                    $log.error(
                        'exception \'' + err.message + '\'' +
                            ' while parsing okc-compare value: ' +
                            '\'' + okcCompare + '\''
                    );
                    ngModel.$setValidity(validationErrorKey, false);
                    return;
                }

                // process comparators for later usage
                comparators.forEach(function (comparator, index) {
                    for (var op in comparator) {
                        // add info to object
                        comparator.element = elem;
                        comparator.inputType = elem[0].type;

                        if (comparator.hasOwnProperty(op)) {
                            // add info to object
                            comparator.operator = op;
                            comparator.nodeSpec = comparator[op];

                            // is op a valid operator?
                            if (validOperators.indexOf(op) < 0) {
                                // invalid operator
                                comparator.error = comparatorErrorMsg(comparator,
                                    'Operator \'' + op + '\' is invalid. ' +
                                        'Must be one of [' + validOperators.join(',') + '].');
                                $log.error(comparator.error);
                                ngModel.$setValidity(validationErrorKey, false);
                                continue;
                            }

                            // node spec format check
                            if (!(comparator[op].indexOf('.') > 0 && comparator[op].split('.').length === 2)) {
                                // malformed comparator
                                comparator.error = comparatorErrorMsg(comparator,
                                    'Must have one and only one separator character \'.\' .');
                                $log.error(comparator.error);
                                ngModel.$setValidity(validationErrorKey, false);
                                continue;
                            }

                            // node resolution check
                            try {
                                comparator.node = angular.element(scope.$eval(comparator.nodeSpec));

                                // did we get empty node?
                                if (comparator.node.length < 1) {
                                    comparator.error = comparatorErrorMsg(elem, op, comparator[op],
                                        'No valid element found using selector \'' + comparator.selector + '\'');
                                    $log.error(comparator.error);
                                    ngModel.$setValidity(validationErrorKey, false);
                                    continue;
                                }
                            }
                            catch (err) {
                                comparator.error = comparatorErrorMsg(elem, op, comparator[op],
                                    'Exception \'' + err.message + '\' resolving selector ' + comparator.selector);
                                $log.error(comparator.error);
                                ngModel.$setValidity(validationErrorKey, false);
                            }
                        }
                    }
                });

                // ignore comparators with errors
                goodComparators = comparators.filter(function (element) {
                    return !element.error;
                });

                //
                // add custom validator function to ngModel.$parsers
                // this is what gets called to do validation
                // it appears this validator is not called if a previous parser sets error - makes sense
                //
                if (goodComparators.length) {
                ngModel.$parsers.unshift(function (viewValue) {
                    var valid = true;
                    // loop over comparator objects performing validity comparisons
                    for (var i = 0; i < goodComparators.length; i++) {
                        // comparator to process
                        var comparator = goodComparators[i];

                        if (!valid) {
                            $log.debug('invalid - skipping subsequent comparators');
                            break;
                        }
                        comparator.viewValue = viewValue;

                        // get view value of target node
                        try {
                            comparator.targetViewValue = comparator.node[0].$viewValue;
                        }
                        catch (err) {
                            comparator.valid = false;
                            $log.error('[okc-compare] exception \'' + err.message +
                                '\' retrieving value from node \'' +
                                comparator.nodeSpec);
                            break;
                        }

                        // normalize/convert values here
                        //   convert strings to numbers for comparison
                        //console.dir(comparator);

                        if (comparator.inputType === 'number') {
                            try {
                                comparator.sourceCompareValue = parseFloat(elem.val());
                                comparator.targetCompareValue = parseFloat(comparator.targetViewValue);
                            }
                            catch (err) {
                                valid = false;
                                $log.log('[okcCompare] exception parsing value ' + err.message);
                                break;
                            }
                        }
                        else {
                            // copy strings to comparator
                            comparator.sourceCompareValue = elem.val();
                            comparator.targetCompareValue = comparator.targetViewValue;
                        }

                        // comparisons occur here
                        // sync the case labels with validOperators list
                        try {
                            switch (comparator.operator) {
                                case '=':
                                case 'eq':
                                    // straight comparison
                                    // valid if target number not set yet?
                                    // TODO verify this check.  Make a helper function.
                                    if (isNaN(comparator.targetCompareValue) && !comparator.targetViewValue.length) {
                                        valid = true;
                                    }
                                    else {
                                        valid = (comparator.sourceCompareValue === comparator.targetCompareValue);
                                    }
                                    break;

                                case '*=':
                                case 'eqo':
                                    // if elem.val is truthy, compare with targetViewValue.
                                    // if elem.val is falsy (empty string), no check needed and comparison is valid.
                                    valid = comparator.sourceCompareValue ?
                                        (comparator.sourceCompareValue === comparator.targetCompareValue) : true;
                                    break;

                                case '!=':
                                case 'ne':
                                    // valid if target number not set yet?
                                    if (isNaN(comparator.targetCompareValue) && !comparator.targetViewValue.length) {
                                        valid = true;
                                    }
                                    else {
                                        valid = (comparator.sourceCompareValue != comparator.targetCompareValue);
                                    }
                                    break;

                                case '<':
                                case 'lt':
                                    // valid if target number not set yet?
                                    if (isNaN(comparator.targetCompareValue) && !comparator.targetViewValue.length) {
                                        valid = true;
                                    }
                                    else {
                                        valid = (comparator.sourceCompareValue < comparator.targetCompareValue);
                                    }
                                    break;

                                case '>':
                                case 'gt':
                                    // valid if target number not set yet?
                                    if (isNaN(comparator.targetCompareValue) && !comparator.targetViewValue.length) {
                                        valid = true;
                                    }
                                    else {
                                        valid = (comparator.sourceCompareValue > comparator.targetCompareValue);
                                    }
                                    break;

                                case '<=':
                                case 'lte':
                                    // valid if target number not set yet?
                                    if (isNaN(comparator.targetCompareValue) && !comparator.targetViewValue.length) {
                                        valid = true;
                                    }
                                    else {
                                        valid = (comparator.sourceCompareValue <= comparator.targetCompareValue);
                                    }
                                    break;

                                case '>=':
                                case 'gte':
                                    // valid if target number not set yet?
                                    if (isNaN(comparator.targetCompareValue) && !comparator.targetViewValue.length) {
                                        valid = true;
                                    }
                                    else {
                                        valid = (comparator.sourceCompareValue >= comparator.targetCompareValue);
                                    }
                                    break;

                                default:
                                    $log.log('unknown operation \'' + comparator.operator + '\' specified.');
                                    break;
                            }
                        }
                        catch (err) {
                            valid = false;
                            $log.error('[okcCompare] ' + err.message);
                            break;
                        }
                    }   // for comparator loop

                    // validity actions
                    if (comparator == null) {
                        $log.debug('[okcCompare] no valid comparators - all must have failed pre-check');
                        ngModel.$setValidity(validationErrorKey, false);
                        return undefined;
                    }
                    else {
                        $log.log('debug: ' +
                            comparator.sourceCompareValue + ' ' + comparator.operator + ' ' + comparator.targetCompareValue +
                            ' : valid =' + valid);
                        ngModel.$setValidity(validationErrorKey, valid);
                        return valid ? viewValue : undefined;
                    }
                });
                }

                // end of link function
                // set valid default=true
                ngModel.$setValidity(validationErrorKey, true);
            }
        };
    });
